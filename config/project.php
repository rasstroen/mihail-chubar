<?php
$config = require_once 'main.php';

if (is_readable(__DIR__ . '/console.php')) {
    $config = array_merge($config, require_once __DIR__ . '/console.php');
}
$config['telegram']['token'] = '225067604:AAF6Jf7Ht0gGRID_kZsM5DZtNq6P-zsxV5U';

$config['social'] = [
    'networks' => [
        \Plumbus\Authorization\Social\SocialNetworkFactory::NETWORK_VKONTAKTE => [
            'enabled' => true,
            'app_id' => env('VK_APP_ID'),
            'app_secret' => env('VK_APP_SECRET'),
            'connect_url' => 'https://' . env('DOMAIN') . '/connect/vk'
        ],
        \Plumbus\Authorization\Social\SocialNetworkFactory::NETWORK_FACEBOOK => [
            'enabled' => true,
            'app_id' => env('FB_APP_ID'),
            'app_secret' => env('FB_APP_SECRET'),
            'connect_url' => 'https://' . env('DOMAIN') . '/connect/fb'
        ],
        \Plumbus\Authorization\Social\SocialNetworkFactory::NETWORK_ODNOKLASSNIKI => [
            'enabled' => true,
            'app_id' => env('OK_APP_ID'),
            'app_secret' => env('OK_APP_SECRET'),
            'app_public' => env('OK_APP_PUBLIC'),
            'connect_url' => 'https://' . env('DOMAIN') . '/connect/ok',
        ],
    ]
];

return $config;