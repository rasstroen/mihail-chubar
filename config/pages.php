<?php
$modules = require_once 'modules.php';

$pages = [
    'index' => new \Plumbus\Core\Controller\Page\SimplePage([
        'layout' => 'index',
        'css' => [],
        'js' => [],
        'title' => '',
        'description' => '',
        'keywords' => '',
        'blocks' => [
            'header' => [],
            'content' => [],
            'sidebar' => []
        ],
    ]),
];
return $pages;
