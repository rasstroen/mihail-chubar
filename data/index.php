<?php
header('X-Accel-Expires: 0');
session_cache_limiter('public');
error_reporting(E_ALL);
ini_set('display_errors', true);
setlocale(LC_TIME, "ru_RU.UTF-8");
date_default_timezone_set('Etc/GMT-3');
/**
 * Подключаем autoLoader
 */
require_once __DIR__ . '/../vendor/autoload.php';
/**
 * Создаем приложение с проектной конфигурацией
 */

$application = new \Plumbus\Core\Application\Web(
    __DIR__ . '/../'
);
/**
 * Запускаем приложение. Вывод будет выведен после обработки запроса.
 */
$application->run();